import re, pprint
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt

from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from django.contrib.auth.models import User
from django.views.decorators.http import require_POST

from .models import *
from .forms import *
from .filters import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
#from post_office import mail
from django.core.mail import send_mail, EmailMessage


def registration_page(request):
	form = CreateUserForm()

	if request.method == 'POST':
		form = CreateUserForm(request.POST)
		if form.is_valid():
			form.save()
			user = form.cleaned_data.get('username')
			messages.success(request, 'Акаунт создан: ' + user)

			return redirect('login')

	context = {'form': form}
	return render(request, 'accounts/registration.html', context)


def login_page(request):
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		
		user = authenticate(request, username=username, password=password )

		if user is not None:
			login(request, user)
			return redirect('home')
		else:
			messages.info(request, 'Логин или пароль неверны')

	context = {}
	return render(request, 'accounts/login.html', context)


def logout_page(request):
	if request.user.is_authenticated:
		logout(request)
	return redirect('home')

def password_reset(request):
	return render(request, 'accounts/password_reset_form.html', context)

def home(request):
	context = {}
	operator = request.GET.get('operator', None)
	filters = {}
	is_rent = request.GET.get('is_rent', '0')
	if is_rent is not None:
		# Починил фильтр, он у вас не правильно был написан
		if is_rent == '0':
			filters['is_rent'] = False
			context['this_page'] = 'buy'
		elif is_rent == '1':
			filters['is_rent'] = True
			context['this_page'] = 'order'
	prefix = request.GET.get('prefix', None)
	if prefix is not None:
		context['prefix'] = prefix
		prefix = prefix.replace("*", ".")
	else:
		prefix = "..."
	term = request.GET.get('term', None)
	if term is not None:
		context['term'] = term
		term = term.replace("-", "").replace("*", ".")
		if len(term) == 7:
			filters['msisdn__regex'] = r'^%s%s' % (prefix, term)
		else:
			filters['msisdn__regex'] = r'^%s' % prefix
	elif prefix is not None:
		filters['msisdn__regex'] = r'^%s' % prefix
	if operator is not None:
		filters['operator'] = [op.__getitem__(0) for op in operators if operator in op].pop()

	item_list = PhoneNumber.objects.filter(**filters).all().order_by('-created')
	top_items = PhoneNumber.objects.filter(is_top=True).all()
	page_size = 20
	paginator = Paginator(item_list, page_size)
	page = request.GET.get('page', 1)
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		items = paginator.page(1)
	except EmptyPage:
		items = paginator.page(paginator.num_pages)
	context['items'] = items
	context["top_items"] = top_items
	return render(request, 'base/index.html', context)


def detail(request, id):
	item = PhoneNumber.objects.get(pk=id)

	context = {'item': item}
	return render(request, 'base/detail.html', context)


def my(request):
	context = {}
	user = request.user
	item_list = PhoneNumber.objects.filter(owner__id=user.pk).order_by('-created').all()
	page_size = 20
	paginator = Paginator(item_list, page_size)
	page = request.GET.get('page', 1)
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		items = paginator.page(1)
	except EmptyPage:
		items = paginator.page(paginator.num_pages)
	context['items'] = items
	return render(request, 'base/my.html', context)


def add(request):
	user = request.user
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = PhoneNumberForm(request.POST, user=user)
		# check whether it's valid:
		if form.is_valid():
			form.save()
			send_mail(
            'Номер опубликован Simkard.kz',
            'Здравствуйте, Вы разместили номер +7 %s на сайте simkard.kz объявление будет активно в течении 90 дней' % form.without_country_code('msisdn'),
            settings.EMAIL_HOST_USER,
            [user.email],
            fail_silently=True,
        	)
			return redirect('/my/')

	# if a GET (or any other method) we'll create a blank form
	else:
		form = PhoneNumberForm(user=user)

	return render(request, 'base/add.html', {'form': form})


def edit(request, id):
	user = request.user
	item = PhoneNumber.objects.get(pk=id, owner=user)
	if request.method == 'POST':
		# create a form instance and populate it with data from the request:
		form = PhoneNumberForm(request.POST, instance=item)
		# check whether it's valid:
		if form.is_valid():
			form.save()
			return redirect('/my/')

	# if a GET (or any other method) we'll create a blank form
	else:
		form = PhoneNumberForm(instance=item)

	return render(request, 'base/edit.html', {'form': form, 'item': item})


def delete(request, id):
	user = request.user
	if request.method == 'POST':
		item = PhoneNumber.objects.filter(pk=id, owner=user).delete()
	return redirect("/my/")


# Обработка страница продавца
def profile_seller(request, id):
	# Получаю профиль продавца через id использовав стандартную модель пользователя
	seller = User.objects.get(id=id)
	numbers = PhoneNumber.objects.filter(owner=seller)
	context = {'seller': seller, 'numbers': numbers}
	return render(request, 'accounts/seller_account.html', context)


# Получение номера телефона
@csrf_exempt
@require_POST   # Обрабатываю только пост запросы
def find_number(request):
	ajax_prefix = request.POST.get('prefix')
	if ajax_prefix == '':
		ajax_prefix = '***'
	ajax_number = request.POST.get('number')
	ajax_prefix = f'{ajax_prefix}'.replace('*', '\\d').strip()
	ajax_number = f'{ajax_number}'.replace('*', '\\d').strip()
	full_number = f'^{ajax_prefix}{ajax_number}'.replace('-', '').strip()
	# Проверка на какой странице находится юзер: Главная, Купить, Аренда.
	# В зависимости от этого фильтровать номера телефонов
	category = request.POST.get('filter')

	if category == 'buy':
		all_numbers = PhoneNumber.objects.filter(is_rent=False).order_by('-created')
	elif category == 'order':
		all_numbers = PhoneNumber.objects.filter(is_rent=True).order_by('-created')
	else:
		all_numbers = PhoneNumber.objects.all().order_by('-created')

	# all_numbers = all_numbers.filter(msisdn__regex=re.compile(full_number))
	reg_full_number = re.compile(full_number)
	find_numbers = []
	for number in all_numbers:

		res_match = reg_full_number.match(str(number.msisdn.national_number))
		# print(res_match)
		if res_match:
			json_number = {
				'operator': number.get_operator_display(),
				'msisdn': number.msisdn_custom_national,
				'price': number.price,
				'is_rent': number.is_rent,
				'link': number.get_absolute_url(),
				'css_class': number.css_class,
			}
			find_numbers.append(json_number)

	return JsonResponse({'find_numbers': find_numbers})

