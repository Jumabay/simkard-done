const methodsWithoutBody = ["GET", "HEAD", "OPTIONS"];
const allowedMethods = ["GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"];

const api = (url, config = {}) =>
  new Promise((resolve, reject) => {
    const formattedReject = (msg) => reject(new Error(msg));
    const formattedMethod = (config?.method || "GET").toUpperCase();

    if (typeof url !== "string") {
      return formattedReject("Incorrect url");
    }

    if (
      typeof configMethod === "string" &&
      !allowedMethods.includes(formattedMethod)
    ) {
      return formattedReject("Invalid method");
    }

    // generate request config
    const { method, headers, body } = (() => {
      const result = {
        headers: {},
      };

      result.method = formattedMethod;
      Object.assign(result.headers, config.headers);

      if (!methodsWithoutBody.includes(formattedMethod)) {
        result.body = config.body;
      }

      return result;
    })();

    const request = new XMLHttpRequest();
    request.open(method, url);

    // set headers
    Object.entries(headers).forEach(([key, value]) => {
      request.setRequestHeader(key, value);
    });

    request.addEventListener("readystatechange", () => {
      if (request.readyState !== request.DONE) {
        return null;
      }

      try {
        const requestHeaders = request.getAllResponseHeaders() || "";
        const { status } = request;

        const responseHeaders = requestHeaders
          .trim()
          .split(/[\r\n]+/)
          .filter((s) => s.length)
          .reduce((result, item) => {
            const [key, value] = item.split(": ");
            if (value) result[key] = value;
            return result;
          }, {});

        resolve({
          method,
          status,
          ok: status >= 200 && status <= 299,
          headers: responseHeaders,
          response: request.responseText,

          text() {
            return this.response;
          },
          json() {
            try {
              return Promise.resolve(JSON.parse(this.response));
            } catch (error) {
              return Promise.reject(new Error("Something went wrong"));
            }
          },
        });

        return requestHeaders;
      } catch (error) {
        formattedReject("Something went wrong");
      }

      return "";
    });

    if (body) request.send(body);
    else request.send();

    return null;
  });
