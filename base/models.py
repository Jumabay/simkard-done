import re
from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
# from phone_field import PhoneField
from phonenumber_field.modelfields import PhoneNumberField
from phonenumber_field.phonenumber import PhoneNumber as OrigPhoneNumber

operators = [
	(1, 'activ'),
	(2, 'kcell'),
	(3, 'altel'),
	(4, 'tele2'),
	(5, 'beeline'),
	(6, 'izi'),
]


class PhoneNumber(models.Model):
	operator = models.PositiveSmallIntegerField('Оператор', choices=operators)
	msisdn = PhoneNumberField('Номер телефона', blank=True, unique=True)
	price = models.IntegerField('Цена')
	cities = [
		(1, 'Алматы'),
		(2, 'Нур-Султан'),
		(3, 'Шымкент'),
		(4, 'Караганда'),
		(5, 'Павлодар'),
		(6, 'Кызылорда'),
		(7, 'Семей'),
		(8, 'Атырау'),
		(9, 'Актобе'),
		(10, 'Актау'),
		(11, 'Уральск'),
		(12, 'Усть-Каменогорск'),
		(13, 'Кокшетау'),
		(14, 'Петропавловск'),
		(15, 'Талдыкорган'),
		(16, 'Жезказган'),
	]

	is_rent = models.BooleanField('Аренда', default=False)

	city = models.PositiveSmallIntegerField('Город', choices=cities)
	description = models.CharField('Описание', max_length=200)
	contact_phone = PhoneNumberField('Контактный телефон', blank=True)
	owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='seller', on_delete=models.CASCADE,)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	active = models.BooleanField(default=False)
	is_top = models.BooleanField('Топ номер', default=False)

	def __str__(self):
		return str(self.msisdn)

	class Meta:
		verbose_name = 'Красивый номер'
		verbose_name_plural = 'Красивые номера' 

	@property
	def css_class(self):
		s = self.get_operator_display()
		return "title__text_%s" % s

	@property
	def price_html(self):
		if self.is_rent:
			return "%d тг/меc" % self.price
		return "%d тг" % self.price

	# Получение ссылки на страницу номера
	def get_absolute_url(self):
		return reverse('detail', args=[self.pk])

	@property
	def msisdn_custom_national(self):
		return self.without_country_code("msisdn")

	@property
	def contact_phone_custom_national(self):
		return self.without_country_code("contact_phone")

	def without_country_code(self, field):
		_field = self.__getattribute__(field)
		if _field and isinstance(_field, OrigPhoneNumber):
			_field = str(_field.national_number)
			return re.sub(r'^(\d{3})(\d{3})(\d{2})(\d{2})$', r'(\1) \2-\3-\4', _field)
		return ''
