from django.urls import path

from . import views

urlpatterns = [
    path('login/', views.login_page, name="login"),
    path('accounts/login/', views.login_page, name="login"),

    path('registration/', views.registration_page, name="registration"),
    path('logout/', views.logout_page, name='logout'),

    path('', views.home, name="home"),
    path('detail/<int:id>/', views.detail, name="detail"),
    path('my/', views.my, name="my"),
    path('add/', views.add, name="add"),
    path('edit/<int:id>/', views.edit, name="edit"),
    path('delete/<int:id>/', views.delete, name="delete"),
    # Страница продавца
    path('seller/<int:id>/', views.profile_seller, name='seller'),
    # Ссылка, куда отправляются пост запросы з поиском номера телефона
    path('find_numbers/', views.find_number, name='find_number')
]
