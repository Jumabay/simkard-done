from django.contrib import admin
from django.urls import path, include 
from django.conf.urls import url
from django.conf.urls.static import static 
from django.conf import settings
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^password_reset/$', auth_views.PasswordResetView.as_view(template_name='accounts/reset_password_form.html')),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(template_name='accounts/password_reset_done.html')),
    path('', include('base.urls')),
    path('comments/', include('fluent_comments.urls')),
    url(r'^', include('django.contrib.auth.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)