from django.apps import AppConfig


class GriduiConfig(AppConfig):
    name = 'gridui'
